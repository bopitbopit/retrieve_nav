const request = require("request");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const url = "https://codequiz.azurewebsites.net/";

request(
  {
    uri: url,
    headers: {
      Cookie: "hasCookie=true",
    },
  },
  function (error, response, body) {
    const dom = new JSDOM(response.body);
    const nodeTRList = dom.window.document.querySelectorAll("tr");
    const nodeList = nodeTRList;
    var arg = process.argv.slice(2);
    for (let i = 1; i < nodeList.length; i++) {
      const nodeTDList = nodeList[i].querySelectorAll("td");
      let position = nodeTDList[0].textContent
      if (position.trim() == arg[0]) {
        return console.log(nodeTDList[1].textContent);
      }
    }
  }
);
